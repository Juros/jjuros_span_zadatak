﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Span_jjuros_task.Models;

namespace Span_jjuros_task.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public PersonController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public JsonResult Get()
        {
            string query = "Select * from dbo.Podaci";
            DataTable table = new DataTable();

            string sqlDataSource = _configuration.GetConnectionString("PersonAppCon");
            SqlDataReader reader;
            using(SqlConnection con = new SqlConnection(sqlDataSource))
            {
                con.Open();
                using(SqlCommand com = new SqlCommand(query, con))
                {
                    reader = com.ExecuteReader();
                    table.Load(reader); ;

                    reader.Close();
                    con.Close();
                }
            }
            LogHelper.Log(DateTime.Now.ToString() + "\t Select from table Podaci was made.", _configuration);
            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(Person person)
        {
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("PersonAppCon");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(sqlDataSource))
            {
                con.Open();
                using (SqlCommand com = new SqlCommand("dbo.SaveEntry", con))
                {
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.Add("@Ime", SqlDbType.VarChar).Value = person.Name;
                    com.Parameters.Add("@Prezime", SqlDbType.VarChar).Value = person.Surname;
                    com.Parameters.Add("@PBroj", SqlDbType.VarChar).Value = person.Postcode;
                    com.Parameters.Add("@Grad", SqlDbType.VarChar).Value = person.City;
                    com.Parameters.Add("@Telefon", SqlDbType.VarChar).Value = person.phoneNumber;

                    reader = com.ExecuteReader();
                    table.Load(reader); ;

                    reader.Close();
                    con.Close();
                }
            }
            LogHelper.Log(DateTime.Now.ToString() + "\t Object successfully inserted into table Podaci.", _configuration);
            return new JsonResult("Procedure executed successfully.");
        }
    }
}
