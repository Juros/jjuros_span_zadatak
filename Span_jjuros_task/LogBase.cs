﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Span_jjuros_task
{
    public abstract class LogBase
    {
        protected readonly object lockObj = new object();
        public abstract void Log(string message, IConfiguration configuration);
    }

    public class Logger : LogBase
    {
        public override void Log(string message, IConfiguration configuration)
        {
            lock (lockObj)
            {
                string query = "Insert into dbo.Logger (Message) values('"+message+"')";
                DataTable table = new DataTable();

                string sqlDataSource = configuration.GetConnectionString("PersonAppCon");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(sqlDataSource))
                {
                    con.Open();
                    using (SqlCommand com = new SqlCommand(query, con))
                    {
                        reader = com.ExecuteReader();
                        table.Load(reader); ;

                        reader.Close();
                        con.Close();
                    }
                }
            }
        }
    }
    public static class LogHelper
    {
        private static LogBase logger = null;
        public static void Log(string message, IConfiguration configuration)
        {
            logger = new Logger();
            logger.Log(message, configuration);
        }
    }
}
