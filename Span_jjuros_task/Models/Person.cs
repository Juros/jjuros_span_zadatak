﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Span_jjuros_task.Models
{
    public class Person
    {
        public int PersonId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }
        public string phoneNumber { get; set; }
    }
}
